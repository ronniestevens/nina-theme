<?php
/**
 * Template Name: Nine Homepage
 * 
 */

get_header('fullwidth');
?>
   
    <section id="one">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-lg-8">
                    <p class="mt2">The relationships we are in in work, life and love are our biggest teachers and the best place for our growth. They bring it all up.</p>
                        
                    <p class="garamond">Are you looking for an experienced guide to help you grow lotus from the struggles you are in?</p>
                    
                    <p>Know yourself in profoundly new ways and lead your relationships toward greater understanding, deeper connection, and richer collaboration.</p>
                    <a href="/connect" class="btn btn-default">Let´s connect</a>

                </div>
            </div>
        </div>
    </section>
    <section id="two">    
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wp-block-media-text alignwide has-media-on-the-right">
                        <figure class="wp-block-media-text__media"><img src="/wp-content/uploads/2020/09/nina_home2_compressed.jpg" alt="Nina Nisar" class="wp-image-210"></figure>
                        <div class="wp-block-media-text__content">
                            <h2 class="garamond fs5">Hi,</h2>
                            <h3 class="oswald">I'm Nina</h3>
                            <p class="garamond-normal">I am a relational leadership coach, organizational culture consultant, and passionate partnership development mentor, dedicated to
                            empowering intentional individuals in the making (we all are!) to grow lotus through the muddy times in our relationships in work, 
                            life and love.<br>
                            <a class="btn btn-default" href="/about">Read more</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  

    <section id="news">
    <div class="container">
            <div class="row justify-content-center">
                <div class="col col-lg-8">

                <h2 class="center">#NEW OFFERING</h2>

                    <p class="inline-quote center">CONFLICT FOR WOMEN</p>
                    <p class="oswald center">Finding Power And Wisdom In Messy Uncertain Times<br>
                    A 9-week learning journey for women who want to grow and evolve</p>
                    <style>
                            </style>
                            <div class='embed-container'>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/f4qFZLAP5Ho" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    
                            </div>
                            <div class="center mt2">
                                <a href="http://nextstagewisdom.com/">http://nextstagewisdom.com/</a>
                            </div>
                </div>
            </div>
        </div>

    </section>

    <section id="four">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-lg-8">
                    <h2 class="center">MY WORK</h2>
                    <div class="center garamond-normal mt1">
                        <p>
                            My work evokes and guides into the often thick struggle of becoming, particularly when the struggle is the other :)
                        </p>
                    </div>

                    <p>
                        I work with leaders, lovers, duos, teams and families to get to the developmental juice at 
                        the center of relationship struggles. Life grows us this way.
                    </p>
                    <p>
                        My approach is informed by cutting edge adult development insight, the integral lens on life itself, 
                        systemic creative conflict engagement work and a deep commitment to amplifying peace and wellbeing between people and within. 
                        I offer guidance and perspective to stimulate a more clarified presence in conflict, more authentic engagement in relationships, 
                        and a richer inner integration to lead others from.
                    </p>
                    <p>
                        I am passionate about inbetween spaces, between people, cultures, traditions, religions, differences. I live and practice in many of those, join me finding your way in yours!
                    </p>
                    <div class="d-flex justify-content-between p-3 mainbuttons">
                        <a href="/personal-guidance" class="btn btn-default">individuals</a>
                        <a href="/duos-couple-work" class="btn btn-default">Duos</a>
                        <a href="/team" class="btn btn-default">Teams</a>
                    </div>  
                    <div class="center garamond-normal mt1">
                      <p>      
                            My work and this site shares in-depth knowledge, resources and inspiration to use the potential in disturbance and conflict well and evolve your capacity to lead and co-create powerfully, from the inside out. 
                        </p>
                        <p>
                            I am right there with you through the inner work that is your next stage of development, well-being, growth, and wilder life.
                        </p>    
                    </div>
                </div>   
            </div>    
        </div>
    </section>
g    
    <section id="three" class="clipping">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <blockquote class="wp-block-quote"><p class="mb0">To be one is always<br> to become with many</p> - Donna J. Haraway</blockquote>
                </div>
            </div>
        </div>
    </section>

    <section id="logos">
    Clients & Partners:<br><br>
        <a href="https://www.ashoka.org/de">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/01ashoka.svg" class="svg three" alt="Ashoka" />
        </a>
        <a href="https://www.unitar.org">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/02unitargrey.png" class="png" alt="Unitar" />
        </a>
        <a href="https://www.gruene-muenchen.de">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/03grune_munchengrey.png" class="png" alt="Gruene Muenchen" />
        </a>
        <a href="https://www.bertelsmann-stiftung.de/de/startseite">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/04Logo_Bertelsmann-Stiftung.svg" class="svg three" alt="Gruene Muenchen" />
        </a>
        <a href="https://www.hfmdk-frankfurt.info/">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/05HfMDK-Logo-rgb-grey.png" class="png" alt="HfMDK" />
        </a>
        <a href="https://fortschrittszentrum.de/de/">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/06fortschrittszentrum-logo.svg" class="svg three" alt="Zentrum für gesellschaftlichen Fortschritt" />
        </a>
        <a href="https://ec.europa.eu/info/index_en">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/07eu_logo--en.svg" class="svg one" alt="European Commission" />    
        </a>
        <a href="https://www.deutschebahn.com/de/">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/Deutsche_Bahn_AG-Logo.svg" class="svg two" alt="Deutsche Bahn" />    
        </a>
        <a href="https://gef.eu">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/08logo-gef.svg" class="svg" alt="GREEN EUROPEAN FOUNDATION" />    
        </a>
        <a href="https://www.start-stiftung.de">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/START_Logo_Grey.png" class="png" alt="START Stiftung" />    
        </a>
        <a href="https://www.hs-furtwangen.de">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/10hfu.svg" class="svg onehalf" alt="Hochschule Furtwangen" />    
        </a>
        <a href="https://www.tuhh.de/tuhh/startseite.html">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/11logo-tuhh-print-de.jpg" class="png" alt="Technische Universität Hamburg Harburg" />    
        </a>
        <a href="https://www.gemeinde-kirchenentwicklung.ekir.de">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/12ZGK.png" class="png" alt="Zentrum Gemeinde- und Kirchenentwicklung" />    
        </a>
        <a href="https://www.lustaufbesserleben.de">
            <img src="/wp-content/themes/nina-theme/inc/assets/img/13logo-labl.png" class="png" alt="LABL Frankfurt" />    
        </a>    

    </section>
    
    <section id="six" class="fixedbg">

    </section>

    <section id="seven">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-lg-8">
                    <h2>Relationship DESIGN</h2>
                    <p class="pl1"><span class='oswald'>CHANGING OUR WAYS OF BEING AND RELATING</span><br>
                    <span class="garamond-normal"> for cultures of extraordinary collaboration, alignment and well being.</span></p>
                </div>
            </div>
        </div>
    </section>


    <section id="eight" class="testimonials">
        <div class="container">

            <div class="row">

                <?php if(function_exists( 'simple_testimonials')) {
                    simple_testimonials();
                }
                ?>

            </div>
        </div>
    </section>
    <section id="lean">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <h2 class="center">Invitation to Lean in</h2>
                    <p>
                    My colleagues Jay Rothman and Rowan Simonsen and I are hosting <a href="www.leaningin.life">a community of practice</a> that empowers us all to lean in when it gets difficult. 
                    </p>
                    <p>     
                    We have all experienced conflict that hurts. So it is natural to react to it with “fight” or “flight”. 
                    We are exploring a fundamentally different way: to engage conflict creatively.
                    </p>
                    <p>         
                    We <a href="http://leaningin.life">invite you to explore with us</a>, how conflict can be a doorway into greater coherence in our lives and relationships.
                    </p>
                    <a class="btn btn-default" href="http://leaningin.life">Learn more</a>
                </div>
                <div class="col-md-6 col-12 vimeo">
                    <div style="padding:56.25% 0 0 0;position:relative;">
                        <iframe src="https://player.vimeo.com/video/283353525" 
                                style="position:absolute;top:0;left:0;width:100%;height:100%;" 
                                frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
                        </iframe>
                    </div>
                    <script src="https://player.vimeo.com/api/player.js"></script>
                  
                </div>
            </div>
        </div>
    </section>

    <section id="nine">
        <div class="container-fluid">
            <!-- <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="qd">„Our ability to transform our conflicts on a personal level will eventually lead to a shared 
                        ability to create a more peaceful and harmonious world." - Diane Museo Hamilton 
                    </div>
                </div>
            </div> -->
        </div>
    </section>

<?php
get_footer('fullwidth');

