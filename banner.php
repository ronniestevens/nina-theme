<?php if(is_front_page() && !get_theme_mod( 'header_banner_visibility' )): ?>
        <div id="page-sub-header" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
            <div class="container">
                <h1>
                    <?php
                    if(get_theme_mod( 'header_banner_title_setting' )){
                        echo get_theme_mod( 'header_banner_title_setting' );
                    }else{
                        echo 'Wordpress + Bootstrap';
                    }
                    ?>
                </h1>
                <p>
                    <?php
                    if(get_theme_mod( 'header_banner_tagline_setting' )){
                        echo get_theme_mod( 'header_banner_tagline_setting' );
                }else{
                        echo esc_html__('To customize the contents of this header banner and other elements of your site, go to Dashboard > Appearance > Customize','wp-bootstrap-starter');
                    }
                    ?>
                </p>
              <hr class="white" >
            </div>
        </div>
    <?php elseif(is_page()): ?>
        <?php if ( has_post_thumbnail() ) {?>
            <div id="page-sub-header" style="background-image:url('<?php the_post_thumbnail_url(); ?>');" >
                <h1>
                    <?php wp_title(''); ?>
                </h1>
                <p> 
                    <?php cc_featured_image_caption(true, false); ?>
                </p>
                <hr class="white">
            </div>
        <?php }; ?>      
    <?php endif; ?>